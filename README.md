# Ejemplo5

Crear una aplicación para calcular honorarios

Calcular montos: líquidos y brutos

Monto de retención es = 11.5

bruto
montoporhora * totalhoras = totalboleta
totalboleta * 0.115 = valorretencion
totalboleta - valorretencion = totalhonorarios

liquido
montoporhora * totalhoras = totalhonorarios
totalhonorarios / 0.885 = totalboleta
totalboleta - totalhonorarios = valorretencion
