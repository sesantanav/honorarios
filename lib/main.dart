import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Honorarios',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Cálculo Honorarios'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

// Definición opciones Radio Button
enum SingingCharacter { liquido, bruto }

//
final txtHoras = TextEditingController();
final txtValorHora = TextEditingController();
final txtHonorarios = TextEditingController();
final txtRetencion = TextEditingController();
final txtTotal = TextEditingController();

class _MyHomePageState extends State<MyHomePage> {
  SingingCharacter? _character; // = SingingCharacter.bruto;

  @override
  void initState() {
    super.initState();
    txtHoras.addListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              child: Padding(
                padding: EdgeInsets.all(15.0),
                child: Text('Seleccionar método cálculo'),
              ),
            ),
            ListTile(
              title: const Text('Líquido'),
              leading: Radio<SingingCharacter>(
                value: SingingCharacter.liquido,
                groupValue: _character,
                onChanged: (SingingCharacter? value) {
                  setState(() {
                    _character = value;
                  });
                },
              ),
            ),
            ListTile(
              title: const Text('Bruto'),
              leading: Radio<SingingCharacter>(
                value: SingingCharacter.bruto,
                groupValue: _character,
                onChanged: (SingingCharacter? value) {
                  setState(() {
                    _character = value;
                  });
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.all(12.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text('Ingresar Horas'),
                  ),
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                          hintText: 'Ingresar', border: OutlineInputBorder()),
                      keyboardType: TextInputType.number,
                      controller: txtHoras,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(12.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text('Ingresar Valor'),
                  ),
                  Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                          hintText: '\$', border: OutlineInputBorder()),
                      keyboardType: TextInputType.number,
                      controller: txtValorHora,
                    ),
                  ),
                ],
              ),
            ),
            TextButton(
              child: Text('Calcular'),
              style: TextButton.styleFrom(
                textStyle: TextStyle(fontSize: 20),
              ),
              onPressed: () {
                setState(() {
                  if (_character.toString() == "SingingCharacter.liquido") {
                    int honorarios =
                        int.parse(txtHoras.text) * int.parse(txtValorHora.text);

                    double total = honorarios / 0.885;
                    double retencion = total - honorarios;

                    txtHonorarios.text = honorarios.toString();
                    txtRetencion.text = retencion.toString();
                    txtTotal.text = total.toString();
                  } else {
                    int total =
                        int.parse(txtHoras.text) * int.parse(txtValorHora.text);
                    double retencion = total * 0.115;
                    double honorarios = total - retencion;

                    txtHonorarios.text = honorarios.toString();
                    txtRetencion.text = retencion.toString();
                    txtTotal.text = total.toString();
                  }
                });
              },
            ),
            SizedBox(
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Text('Cálculo de honorarios con retención = 11.5%'),
              ),
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text('Total Honorarios: '),
                ),
                Expanded(
                  child: TextField(
                    decoration: InputDecoration.collapsed(
                        hintText: '\$', border: OutlineInputBorder()),
                    keyboardType: TextInputType.number,
                    controller: txtHonorarios,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text('Retención: '),
                ),
                Expanded(
                  child: TextField(
                    decoration: InputDecoration.collapsed(
                        hintText: '\$', border: OutlineInputBorder()),
                    keyboardType: TextInputType.number,
                    controller: txtRetencion,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: Text('Total Boletaa :'),
                ),
                Expanded(
                  child: TextField(
                    decoration: InputDecoration.collapsed(
                        hintText: '\$', border: OutlineInputBorder()),
                    keyboardType: TextInputType.number,
                    controller: txtTotal,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
